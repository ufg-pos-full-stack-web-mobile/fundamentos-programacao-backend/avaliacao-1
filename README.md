# Projeto usando JDBC

## Avaliação 1  
Implementação de operações CRUD com JDBC


## Como utilizar:

### Compilando o projeto

Para compilar o projeto basta usar o camando install do Maven na pasta raiz do projeto:
> ~$ mvn clean install

O projeto vai ser compilado. Será gerado a pasta target, onde, dentro estará o JAR executável da aplicação.

### Criando o banco de dados:
1. É preciso ter uma banco PostgresSQL local, cujo usuário seja 'postgres' e a senha seja 'postgres'  
2. Em uma base de dados local PostgreSQL crie o banco de dados com nome bd-produto;  
3. Crie a tabela produto. Basta executar o seguinte Script:


    > CREATE TABLE produto (  
       id SERIAL PRIMARY KEY,  
       nome VARCHAR  
      );

### Como executar a aplicação

Basta executar o arquivo JAR na linha de comando

> ~$ java -jar avaliacao-1-1.0.0-SNAPSHOT.jar



## End-points disponibilizados
Os seguintes End-Points serão disponibilizados
* Buscar todos os produtos do banco  
    Método: GET  
    > http://localhost:8080/produto/todos
    
* Buscar produto por ID  
    Método: GET  
    > http://localhost:8080/produto/1
      
    O último parâmetro é um inteiro que pode ser modificado.
    
* Criar novo Produto  
    Método: PUT  
    > http://localhost:8080/produto/novo  
    
    Esse end-point recebe um JSON no seguinte formato:  
    > { "nome": "Nome Produto" }
    
* Atualizar um Produto existente  
    Método: POST  
    > http://localhost:8080/produto/atualizar  
    
    Esse end-point recebe um JSON no seguinte formato:  
    > { "id": 5,"nome": "Nome Produto" }
    
    O atríbuto ID define qual produto no banco de dados será atualizado;
    
* Excluir um Produto  
    Método: DELETE  
    > http://localhost:8080/produto/3
    
    O último parâmetro é um inteiro que pode ser modificado e define qual produto será excluído.