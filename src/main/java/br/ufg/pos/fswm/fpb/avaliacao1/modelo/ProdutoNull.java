package br.ufg.pos.fswm.fpb.avaliacao1.modelo;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 15/07/17.
 */
public class ProdutoNull extends Produto {
    public ProdutoNull(Integer id) {
        super(id, "Não existe produto com id " + id);
    }
}
