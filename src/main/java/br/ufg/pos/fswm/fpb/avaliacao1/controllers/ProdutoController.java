package br.ufg.pos.fswm.fpb.avaliacao1.controllers;

import br.ufg.pos.fswm.fpb.avaliacao1.modelo.Produto;
import br.ufg.pos.fswm.fpb.avaliacao1.servico.ProdutoServico;
import br.ufg.pos.fswm.fpb.avaliacao1.servico.ProdutoServicoImpl;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 15/07/17.
 */
@RestController
@RequestMapping("/produto")
public class ProdutoController {

    private ProdutoServico produtoServico;

    public ProdutoController() {
        this.produtoServico = new ProdutoServicoImpl();
    }

    @GetMapping("/todos")
    public List<Produto> buscarTodos() {
        return produtoServico.buscarTodos();
    }

    @GetMapping("/{id}")
    public Produto buscarPorId(@PathVariable("id") Integer id) {
        return produtoServico.buscarPorId(id);
    }

    @PutMapping("/novo")
    public void criarNovo(@RequestBody Produto produto) {
        produtoServico.salvar(produto);
    }

    @PostMapping("/atualizar")
    public void atualizar(@RequestBody Produto produto) {
        produtoServico.atualizar(produto);
    }

    @DeleteMapping("/{id}")
    public void excluir(@PathVariable("id") Integer id) {
        produtoServico.excluir(id);
    }
}
