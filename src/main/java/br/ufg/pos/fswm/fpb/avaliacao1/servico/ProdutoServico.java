package br.ufg.pos.fswm.fpb.avaliacao1.servico;

import br.ufg.pos.fswm.fpb.avaliacao1.modelo.Produto;

import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 15/07/17.
 */
public interface ProdutoServico {

    void salvar(Produto produto);

    Produto buscarPorId(Integer id);

    List<Produto> buscarTodos();

    void atualizar(Produto produto);

    Produto excluir(Integer id);
}
