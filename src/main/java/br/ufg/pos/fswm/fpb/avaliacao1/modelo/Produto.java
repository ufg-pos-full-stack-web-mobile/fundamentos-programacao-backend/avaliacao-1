package br.ufg.pos.fswm.fpb.avaliacao1.modelo;

import java.io.Serializable;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 15/07/17.
 */
public class Produto implements Serializable {

    private Integer id;
    private String nome;

    public Produto() {
    }

    public Produto(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
