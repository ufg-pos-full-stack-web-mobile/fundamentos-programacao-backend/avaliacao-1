package br.ufg.pos.fswm.fpb.avaliacao1.dao;

import br.ufg.pos.fswm.fpb.avaliacao1.modelo.Produto;

import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 15/07/17.
 */
public interface ProdutoDAO {

    void salvar(Produto produto);

    Produto buscarPorId(Integer id);

    List<Produto> buscarTodos();

    void atualizar(Produto produto);

    void excluir(Integer id);
}
