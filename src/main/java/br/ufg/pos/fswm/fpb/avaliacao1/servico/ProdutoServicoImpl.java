package br.ufg.pos.fswm.fpb.avaliacao1.servico;

import br.ufg.pos.fswm.fpb.avaliacao1.dao.ProdutoDAO;
import br.ufg.pos.fswm.fpb.avaliacao1.dao.ProdutoDAOImpl;
import br.ufg.pos.fswm.fpb.avaliacao1.modelo.Produto;
import br.ufg.pos.fswm.fpb.avaliacao1.modelo.ProdutoNull;

import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 15/07/17.
 */
public class ProdutoServicoImpl implements ProdutoServico {

    private ProdutoDAO produtoDAO;

    public ProdutoServicoImpl() {
        this.produtoDAO = new ProdutoDAOImpl();
    }

    @Override
    public void salvar(Produto produto) {
        if (produto.getId() != null) {
            atualizar(produto);
            return;
        }
        produtoDAO.salvar(produto);
    }

    @Override
    public Produto buscarPorId(Integer id) {
        Produto produto = produtoDAO.buscarPorId(id);

        if (produto == null) {
            produto = new ProdutoNull(id);
        }

        return produto;
    }

    @Override
    public List<Produto> buscarTodos() {
        return produtoDAO.buscarTodos();
    }

    @Override
    public void atualizar(Produto produto) {
        if (produto.getId() == null) {
            salvar(produto);
            return;
        }

        produtoDAO.atualizar(produto);
    }

    @Override
    public Produto excluir(Integer id) {
        Produto produto = buscarPorId(id);

        if (!(produto instanceof ProdutoNull)) {
            produtoDAO.excluir(id);
        }

        return produto;
    }
}
