package br.ufg.pos.fswm.fpb.avaliacao1.dao;

import br.ufg.pos.fswm.fpb.avaliacao1.modelo.Produto;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 15/07/17.
 */
public class ProdutoDAOImpl implements ProdutoDAO {

    @Override
    public void salvar(Produto produto) {
        Connection conn = null;
        PreparedStatement pstmt = null;

        try {
            conn = JdbcUtil.getConnection();

            final String sql = "INSERT INTO produto (nome) VALUES (?)";

            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, produto.getNome());

            pstmt.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtil.close(conn, pstmt);
        }
    }

    @Override
    public Produto buscarPorId(Integer id) {
        Produto produto = null;

        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;

        try {
            conn = JdbcUtil.getConnection();

            final String sql = "SELECT p.id, p.nome FROM produto p WHERE p.id = ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);

            rs = pstmt.executeQuery();

            if (rs.next()) {
                final Integer identificador = rs.getInt("id");
                final String nome = rs.getString("nome");

                produto = new Produto(identificador, nome);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtil.close(conn, rs, pstmt);
        }

        return produto;
    }

    @Override
    public List<Produto> buscarTodos() {
        final List<Produto> produtos = new ArrayList<>();

        Connection conn = null;
        ResultSet rs = null;
        Statement stmt = null;

        try {
            conn = JdbcUtil.getConnection();

            final String sql = "SELECT p.id, p.nome FROM produto p";

            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                final Integer id = rs.getInt("id");
                final String nome = rs.getString("nome");

                final Produto produto = new Produto(id, nome);
                produtos.add(produto);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtil.close(conn, rs, stmt);
        }

        return produtos;
    }

    @Override
    public void atualizar(Produto produto) {
        Connection conn = null;
        PreparedStatement pstmt = null;

        try {
            conn = JdbcUtil.getConnection();

            final String sql = "UPDATE produto SET nome = ? WHERE id = ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, produto.getNome());
            pstmt.setInt(2, produto.getId());

            pstmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtil.close(conn, pstmt);
        }

    }

    @Override
    public void excluir(Integer id) {
        Connection conn = null;
        PreparedStatement pstmt = null;

        try {
            conn = JdbcUtil.getConnection();

            final String sql = "DELETE FROM produto WHERE id = ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);

            pstmt.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtil.close(conn, pstmt);
        }
    }
}
